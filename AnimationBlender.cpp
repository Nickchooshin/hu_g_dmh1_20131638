#include "AnimationBlender.h"

void AnimationBlender::init(const Ogre::String &animation, bool l)
{
	Ogre::AnimationStateSet *set = m_pEntity->getAllAnimationStates() ;
	Ogre::AnimationStateIterator it = set->getAnimationStateIterator() ;

	while(it.hasMoreElements())
	{
		Ogre::AnimationState *anim = it.getNext() ;
		anim->setEnabled(false) ;
		anim->setWeight(0) ;
		anim->setTimePosition(0) ;
	}

	m_pSource = m_pEntity->getAnimationState(animation) ;
	m_pSource->setEnabled(true) ;
	m_pSource->setWeight(1) ;
	m_Timeleft = 0 ;
	m_Duration = 1 ;
	m_pTarget = 0 ;
	complete = false ;
	loop = l ;
}

void AnimationBlender::blend(const Ogre::String &animation, BlendingTransition transition, Ogre::Real duration, bool l)
{
	loop = l ;

	if(transition==AnimationBlender::BlendSwitch)
	{
		if(m_pSource!=0)
			m_pSource->setEnabled(false) ;
		m_pSource = m_pEntity->getAnimationState(animation) ;
		m_pSource->setEnabled(true) ;
		m_pSource->setWeight(1) ;
		m_pSource->setTimePosition(0) ;
		m_Timeleft = 0 ;
	}
	else
	{
		Ogre::AnimationState *newTarget = m_pEntity->getAnimationState(animation) ;

		if(m_Timeleft>0)
		{
			if(newTarget==m_pTarget)
			{
			}
			else if(newTarget==m_pSource)
			{
				// going back to the source state, so let's switch
				m_pSource = m_pTarget ;
				m_pTarget = newTarget ;
				m_Timeleft = m_Duration - m_Timeleft ;
			}
			else
			{
				if(m_Timeleft<m_Duration*0.5)
				{
					m_pTarget->setEnabled(false) ;
					m_pTarget->setWeight(0) ;
				}
				else
				{
					m_pSource->setEnabled(true) ;
					m_pSource->setWeight(0) ;
					m_pSource = m_pTarget ;
				}

				m_pTarget = newTarget ;
				m_pTarget->setEnabled(true) ;
				m_pTarget->setWeight(1.0 - m_Timeleft / m_Duration) ;
				m_pTarget->setTimePosition(0) ;
			}
		}
		else
		{
			m_Transition = transition ;
			m_Timeleft = m_Duration = duration ;
			m_pTarget->setEnabled(true) ;
			m_pTarget->setWeight(0) ;
			m_pTarget->setTimePosition(0) ;
		}
	}
}

void AnimationBlender::addTime(Ogre::Real time)
{
}