#pragma once

#include <Ogre.h>

class AnimationBlender
{
public :
	enum BlendingTransition
	{
		BlendSwitch,
		BlendWhileAnimation,
		BlendThenAnimate
	} ;

private :
	Ogre::Entity *m_pEntity ;
	Ogre::AnimationState *m_pSource ;
	Ogre::AnimationState *m_pTarget ;

	BlendingTransition m_Transition ;

	bool loop ;

	~AnimationBlender() {}

public :
	Ogre::Real m_Timeleft, m_Duration ;

	bool complete ;

	void init(const Ogre::String &animation, bool l) ;

	void blend(const Ogre::String &animation, BlendingTransition transition, Ogre::Real duration, bool l=true) ;
	void addTime(Ogre::Real) ;
	Ogre::Real getProgress() { return m_Timeleft / m_Duration ; }
	Ogre::AnimationState* getSource() { return m_pSource ; }
	Ogre::AnimationState* getTarget() { return m_pTarget ; }
	AnimationBlender(Ogre::Entity*) ;
	void Init(const Ogre::String &animation, bool l=true) ;
} ;