#include <Ogre.h>
#include <OIS.h>
#include <OgreOverlaySystem.h>

#include "GameManager.h"
#include "GameState.h"

using namespace Ogre ;

GameManager::GameManager()
{
	m_pRoot = 0 ;
}
GameManager::~GameManager()
{
	while(!states.empty())
	{
		states.back()->exit() ;
		states.pop_back() ;
	}

	if(m_pRoot)
		delete m_pRoot ;
}

void GameManager::init(void)
{
	m_pRoot = new Root("plugins_d.cfg", "ogre.cfg", "ogre.log") ;

	if(!configure()) return ;

	//setupResource() ;
	m_pRoot->addFrameListener(this) ;

	size_t hWnd=0 ;
	m_pWindow->getCustomAttribute("WINDOW", &hWnd) ;

	m_pInputManager = OIS::InputManager::createInputSystem(hWnd) ;
	m_pKeyboard = static_cast<OIS::Keyboard*>(m_pInputManager->createInputObject(OIS::OISKeyboard, true)) ;
	m_pMouse = static_cast<OIS::Mouse*>(m_pInputManager->createInputObject(OIS::OISMouse, true)) ;
	
	Ogre::SceneManager *pSceneMgr = m_pRoot->createSceneManager(ST_GENERIC, "main") ;

	Ogre::OverlaySystem *pOverlayManager = new Ogre::OverlaySystem() ;
	pSceneMgr->addRenderQueueListener(pOverlayManager) ;

	m_pKeyboard->setEventCallback(this) ;
	m_pMouse->setEventCallback(this) ;

	setupResource() ;
}

void GameManager::go(void)
{
	if(m_pRoot)
		m_pRoot->startRendering() ;
}

void GameManager::changeState(GameState* state)
{
	if(!states.empty())
	{
		states.back()->exit() ;
		states.pop_back() ;
	}
	states.push_back(state) ;
	states.back()->enter() ;
}

void GameManager::pushState(GameState* state)
{
	if(!states.empty())
	{
		states.back()->pause() ;
	}
	states.push_back(state) ;
	states.back()->enter() ;
}

void GameManager::popState()
{
	if(!states.empty())
	{
		states.back()->exit() ;
		states.pop_back() ;
	}

	if(!states.empty())
	{
		states.back()->resume() ;
	}
}

void GameManager::setupResource(void)
{
	ResourceGroupManager::getSingleton().addResourceLocation("./Resource.zip", "Zip") ;
	ResourceGroupManager::getSingleton().addResourceLocation("./Resource(1).zip", "Zip") ;
	ResourceGroupManager::getSingleton().addResourceLocation("./illidan.zip", "Zip") ;
	ResourceGroupManager::getSingleton().initialiseAllResourceGroups() ;
}

bool GameManager::configure(void)
{
	if(!m_pRoot->restoreConfig())
	{
		if(m_pRoot->showConfigDialog())
		{
			m_pWindow = m_pRoot->initialise(true) ;
			return true ;
		}
	}

	m_pWindow = m_pRoot->initialise(true) ;

	return true ;
}

bool GameManager::frameStarted(const FrameEvent& evt)
{
	if(m_pMouse)
		m_pMouse->capture() ;

	if(m_pKeyboard)
		m_pKeyboard->capture() ;

	return states.back()->frameStarted(this, evt) ;
}

bool GameManager::frameEnded(const FrameEvent& evt)
{
	return states.back()->frameEnded(this, evt) ;
}

bool GameManager::mouseMoved(const OIS::MouseEvent &e)
{
	return states.back()->mouseMoved(this, e) ;
}

bool GameManager::mousePressed(const OIS::MouseEvent &e, const OIS::MouseButtonID id)
{
	return states.back()->mousePressed(this, e, id) ;
}

bool GameManager::mouseReleased(const OIS::MouseEvent &e, const OIS::MouseButtonID id)
{
	return states.back()->mouseReleased(this, e, id) ;
}

bool GameManager::keyPressed(const OIS::KeyEvent &e)
{
	return states.back()->keyPressed(this, e) ;
}

bool GameManager::keyReleased(const OIS::KeyEvent &e)
{
	return states.back()->keyReleased(this, e) ;
}