#ifndef GameManager_H
#define GameManager_H

#include <vector>
#include <Ogre.h>
#include <OIS.h>

class GameState ;

class GameManager : public Ogre::FrameListener, public OIS::KeyListener, public OIS::MouseListener
{
public :
	GameManager() ;
	~GameManager() ;

	void init(void) ;

	void changeState(GameState* state) ;
	void pushState(GameState* state) ;
	void popState() ;

	void go(void) ;

	bool mouseMoved(const OIS::MouseEvent &e) ;
	bool mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) ;
	bool mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) ;

	bool keyPressed(const OIS::KeyEvent &e) ;
	bool keyReleased(const OIS::KeyEvent &e) ;

protected :
	Ogre::Root*			m_pRoot ;
	Ogre::RenderWindow*	m_pWindow ;

	OIS::Keyboard*		m_pKeyboard ;
	OIS::Mouse*			m_pMouse ;
	OIS::InputManager*	m_pInputManager ;

	void setupResource(void) ;
	bool configure(void) ;
	bool frameStarted(const Ogre::FrameEvent& evt) ;
	bool frameEnded(const Ogre::FrameEvent& evt) ;

private :
	std::vector<GameState*> states ;
} ;

#endif