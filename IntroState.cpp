#include <Ogre.h>
#include <OIS.h>

#include "IntroState.h"
#include "PlayState.h"

using namespace Ogre ;
using namespace OIS ;

IntroState IntroState::m_IntroState ;

void IntroState::enter()
{
	m_pRoot = Root::getSingletonPtr() ;

	m_pSceneMgr = m_pRoot->getSceneManager("main") ;
	m_pCamera = m_pSceneMgr->createCamera("IntroCamera") ;
	m_pViewport = m_pRoot->getAutoCreatedWindow()->addViewport(m_pCamera) ;
	m_pViewport->setBackgroundColour(ColourValue(1.0, 0.0, 0.0)) ;

	MaterialPtr material = MaterialManager::getSingleton().create("Background", "General") ;
	material->getTechnique(0)->getPass(0)->createTextureUnitState("introstatebackground.bmp") ;
	material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false) ;
	material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false) ;
	material->getTechnique(0)->getPass(0)->setLightingEnabled(false) ;

	Rectangle2D* rect = new Rectangle2D(true) ;
	rect->setCorners(-1.0, 1.0, 1.0, -1.0) ;
	rect->setMaterial("Background") ;

	rect->setRenderQueueGroup(RENDER_QUEUE_BACKGROUND) ;

	SceneNode* node = m_pSceneMgr->getRootSceneNode()->createChildSceneNode("Background") ;
	node->attachObject(rect) ;

	m_bContinue = true ;
}

void IntroState::exit()
{
	MaterialManager::getSingleton().remove("Background") ;
	m_pSceneMgr->clearScene() ;
	m_pSceneMgr->destroyAllCameras() ;
	m_pRoot->getAutoCreatedWindow()->removeAllViewports() ;
}

void IntroState::pause()
{
	std::cout << "IntroState pause\n" ;
}

void IntroState::resume()
{
	std::cout << "IntroState resume\n" ;
}

bool IntroState::frameStarted(GameManager* game, const FrameEvent& evt)
{
	return true ;
}

bool IntroState::frameEnded(GameManager* game, const FrameEvent& evt)
{
	return m_bContinue ;
}

bool IntroState::mouseMoved(GameManager* game, const OIS::MouseEvent &e)
{
	return true ;
}

bool IntroState::mousePressed(GameManager* game, const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	return true ;
}

bool IntroState::mouseReleased(GameManager* game, const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	return true ;
}
	
bool IntroState::keyPressed(GameManager* game, const OIS::KeyEvent &e)
{
	switch(e.key)
	{
	case OIS::KC_SPACE : game->changeState(PlayState::getInstance()) ; break ;
	case OIS::KC_ESCAPE :m_bContinue = false ; break ;
	}

	return true ;
}

bool IntroState::keyReleased(GameManager* game, const OIS::KeyEvent &e)
{
	switch(e.key)
	{
	case OIS::KC_ESCAPE : return false ;
	}

	return true ;
}