#ifndef IntroState_H
#define IntroState_H

#include <Ogre.h>
#include "GameState.h"

class IntroState : public GameState
{
	static IntroState m_IntroState ;

public :
	void enter() ;
	void exit() ;

	void pause() ;
	void resume() ;

	bool frameStarted(GameManager* game, const Ogre::FrameEvent& evt) ;
	bool frameEnded(GameManager* game, const Ogre::FrameEvent& evt) ;
	
	bool mouseMoved(GameManager* game, const OIS::MouseEvent &e) ;
	bool mousePressed(GameManager* game, const OIS::MouseEvent &e, OIS::MouseButtonID id) ;
	bool mouseReleased(GameManager* game, const OIS::MouseEvent &e, OIS::MouseButtonID id) ;
	
	bool keyPressed(GameManager* game, const OIS::KeyEvent &e) ;
	bool keyReleased(GameManager* game, const OIS::KeyEvent &e) ;

	static IntroState* getInstance() { return &m_IntroState ; }

	bool m_bContinue ;

protected :
	IntroState() {}

	Ogre::Root*			m_pRoot ;
	Ogre::SceneManager*	m_pSceneMgr ;
	Ogre::Viewport*		m_pViewport ;
	Ogre::Camera*		m_pCamera ;
} ;

#endif