#include "GameManager.h"
#include "IntroState.h"

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#endif

#ifdef _cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	int WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, int)
#else
	int main(int agrc, char *argv[])
#endif
	{
		GameManager game ;

		try {
			game.init() ;
			game.changeState(IntroState::getInstance()) ;
			game.go() ;
		} catch(Ogre::Exception& e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An esception has occured!", MB_OK | MB_ICONERROR | MB_TASKMODAL) ;
#else
			std::cerr << "An exception has occured: " << e.getFullDescription().c_str() << std::endl ;
#endif
		}

		return 0 ;
	}
#ifdef __cplusepluse
}
#endif