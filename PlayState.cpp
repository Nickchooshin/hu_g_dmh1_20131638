#include <Ogre.h>
#include <OIS.h>

#include "PlayState.h"
#include "IntroState.h"

using namespace Ogre ;
using namespace OIS ;

PlayState PlayState::m_PlayState ;

void PlayState::enter()
{
	m_pRoot = Root::getSingletonPtr() ;

	m_pSceneMgr = m_pRoot->getSceneManager("main") ;

	// 카메라 실습 : 4개의 Viewport 구현
	m_pCamera = m_pSceneMgr->createCamera("Camera") ;

	m_pViewport = m_pRoot->getAutoCreatedWindow()->addViewport(m_pCamera, 1, 0, 0, 1, 1) ;
	m_pViewport->setBackgroundColour(ColourValue(0, 0, 0)) ;
	
	// 광원 설정
	m_pSceneMgr->setAmbientLight(ColourValue(1.0f, 1.0f, 1.0f)) ;

	// 그림자 테크닉 설정
	m_pSceneMgr->setShadowTechnique(SHADOWTYPE_STENCIL_ADDITIVE) ;

	// 평면 메쉬의 생성
	Plane plane(Ogre::Vector3::UNIT_Y, 0) ;

	MeshManager::getSingleton().createPlane(
		"Ground",
		ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
		plane,
		500, 500,
		1, 1,
		true, 1, 5, 5,
		Ogre::Vector3::NEGATIVE_UNIT_Z) ;

	// Ground 엔티티 및 장면노드 생성
	Entity* groundEntity = m_pSceneMgr->createEntity("GroundPlane", "Ground") ;
	m_pSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(groundEntity) ;
	groundEntity->setMaterialName("rockwall") ;
	groundEntity->setCastShadows(false) ;
	
	// 일리단 씬 노드 구성
	m_pillidanNode = m_pSceneMgr->getRootSceneNode()->createChildSceneNode("illidan") ;
	m_pillidanYaw = m_pillidanNode->createChildSceneNode("illidanYaw") ;

	// 카메라 씬 노드 구성
	m_pCameraYaw = m_pillidanNode->createChildSceneNode("CameraYaw", Ogre::Vector3(0.0f, 120.0f, 0.0f)) ;
	m_pCameraPitch = m_pCameraYaw->createChildSceneNode("CameraPitch") ;
	m_pCameraHolder = m_pCameraPitch->createChildSceneNode("CameraHolder", Ogre::Vector3(0.0f, 80.0f, 500.0f)) ;

	m_pCameraYaw->setInheritOrientation(false) ;

	// 엔티티 생성
	m_pillidanEntity = m_pSceneMgr->createEntity("illidan", "illidan.mesh") ;
	m_pillidanEntity->setCastShadows(true) ;
	
	//m_pillidanNode->attachObject(m_pillidanEntity) ;
	m_pillidanYaw->attachObject(m_pillidanEntity) ;

	//m_pillidanNode->setScale(Ogre::Vector3(20.0f, 20.0f, 20.0f)) ;
	m_pillidanYaw->setScale(Ogre::Vector3(10.0f, 10.0f, 10.0f)) ;
	m_pillidanYaw->yaw(Degree(270)) ;

	m_pCameraHolder->attachObject(m_pCamera) ;
	m_pCamera->lookAt(m_pCameraYaw->getPosition()) ;

	// 애니메이션 상태 설정
	m_pAnimationState_Walk = m_pillidanEntity->getAnimationState("Walk") ;
	m_pAnimationState_Stand = m_pillidanEntity->getAnimationState("Stand") ;
	m_pAnimationState_Attack = m_pillidanEntity->getAnimationState("AttackUnarmed") ;
	m_pAnimationState_Death = m_pillidanEntity->getAnimationState("Death") ;
	
	m_pAnimationState_Walk->setLoop(true) ;
	m_pAnimationState_Stand->setLoop(true) ;
	//m_pAnimationState_Attack->setLoop(true) ;
	//m_pAnimationState_Death->setLoop(true) ;
	
	m_pAnimationState_Walk->setEnabled(false) ;
	m_pAnimationState_Stand->setEnabled(true) ;
	m_pAnimationState_Attack->setEnabled(false) ;
	m_pAnimationState_Death->setEnabled(false) ;

	m_CharacterDirection = Ogre::Vector3::ZERO ;

	m_bAttacking = false ;
	m_bDeading = false ;

	// 실습 (1) - 텍스트 문자 출력
	m_pOverlayMgr = Ogre::OverlayManager::getSingletonPtr() ;

	m_pTextOverlay = m_pOverlayMgr->create("TextOverlay") ;
	m_pTextOverlayContainer = static_cast<Ogre::OverlayContainer*>(m_pOverlayMgr->createOverlayElement("Panel", "MyTextOverlayElement")) ;
	m_pTextOverlayContainer->setMetricsMode(Ogre::GMM_PIXELS) ;
	m_pTextOverlayContainer->setDimensions(1, 1) ;
	m_pTextOverlayContainer->setPosition(30, 30) ;

	m_pTextOverlayElement = m_pOverlayMgr->createOverlayElement("TextArea", "TextID") ;
	m_pTextOverlayElement->setMetricsMode(Ogre::GMM_PIXELS) ;
	m_pTextOverlayElement->setPosition(10, 10) ;
	m_pTextOverlayElement->setWidth(100) ;
	m_pTextOverlayElement->setHeight(20) ;
	m_pTextOverlayElement->setCaption(L"Hello, World!") ;
	m_pTextOverlayElement->setParameter("char_height", "60") ;
	m_pTextOverlayElement->setParameter("font_name", "Font/Malgun") ;
	m_pTextOverlayElement->setColour(Ogre::ColourValue(1.0, 0.0, 0.0)) ;

	m_pTextOverlayContainer->addChild(m_pTextOverlayElement) ;
	m_pTextOverlay->add2D(m_pTextOverlayContainer) ;
	m_pTextOverlay->show() ;

	// 실습 (2) - 로고 출력
	m_pLogoOverlayContainer = static_cast<Ogre::OverlayContainer*>(m_pOverlayMgr->createOverlayElement("Panel", "container1")) ;
	m_pLogoOverlayContainer->setMetricsMode(Ogre::GMM_PIXELS) ;
	m_pLogoOverlayContainer->setDimensions(1, 1) ;
	m_pLogoOverlayContainer->setPosition(-3.0f, 0.5f) ;
	m_pLogoOverlay = m_pOverlayMgr->getByName("HoseoLogo") ;
	m_pLogoOverlay->show() ;

	// 실습 (3) - FPS 출력
	m_pFpsOverlay = Ogre::OverlayManager::getSingleton().getByName("Information") ;
	m_pFpsOverlay->show() ;

	//
	//m_pSceneMgr->setWorldGeometry("terrain.cfg") ;
	//m_pSceneMgr->setSkyBox(true, "Examples/SpaceSkyBox") ;
	//m_pSceneMgr->setSkyDome(true, "Examples/CloudySky", 2, 8) ;

	Plane _plane ;
	_plane.d = 1000 ;
	_plane.normal = Ogre::Vector3::NEGATIVE_UNIT_Y ;
	m_pSceneMgr->setSkyPlane(true, _plane, "Examples/CloudySky", 1500, 75) ;

	m_bContinue = true ;
}

void PlayState::exit()
{
	m_pSceneMgr->clearScene() ;
	m_pSceneMgr->destroyAllCameras() ;
	m_pRoot->getAutoCreatedWindow()->removeAllViewports() ;

	std::cout << "PlayState exit\n" ;
}

void PlayState::pause()
{
}

void PlayState::resume()
{
}

bool PlayState::frameStarted(GameManager* game, const FrameEvent& evt)
{
	if(m_bAttacking)
	{
		if(m_pAnimationState_Attack->getTimePosition() >= m_pAnimationState_Attack->getLength())
		{
			m_pAnimationState_Stand->setEnabled(true) ;
			m_pAnimationState_Stand->setTimePosition(0.0) ;
			m_bAttacking = false ;
			m_pAnimationState_Attack->setEnabled(false) ;
			m_pAnimationState_Attack->setTimePosition(0.0) ;
		}
	}
	if(m_bDeading)
	{
		if(m_pAnimationState_Death->getTimePosition() >= m_pAnimationState_Death->getLength())
		{
			m_pAnimationState_Stand->setEnabled(true) ;
			m_pAnimationState_Stand->setTimePosition(0.0) ;
			m_bDeading = false ;
			m_pAnimationState_Death->setEnabled(false) ;
			m_pAnimationState_Death->setTimePosition(0.0) ;
		}
	}

	if(m_bAttacking)
		m_pAnimationState_Attack->addTime(evt.timeSinceLastFrame) ;
	else if(m_bDeading)
		m_pAnimationState_Death->addTime(evt.timeSinceLastFrame) ;
	else
	{
		if(m_CharacterDirection!=Ogre::Vector3::ZERO)
		{
			// 방향 전환 운동이 시작될 때, 캐릭터의 축을 카메라의 축과 align
			m_pillidanNode->setOrientation(m_pCameraYaw->getOrientation()) ;

			// 캐릭터의 바라 보는 방향을 전환
			Quaternion quat = Ogre::Vector3(Ogre::Vector3::UNIT_Z).getRotationTo(m_CharacterDirection) ;
			m_pillidanYaw->setOrientation(quat) ;

			m_pillidanYaw->yaw(Degree(270)) ;

			// 캐릭터 축을 기준으로 하여, 캐릭터 이동(속도 : 111cm/sec = 약 4km/sec)
			// Character Root의 local space 를 기준으로 이동
			m_pillidanNode->translate(m_CharacterDirection.normalisedCopy() * 111 * evt.timeSinceLastFrame, Node::TransformSpace::TS_LOCAL) ;

			if(!m_pAnimationState_Walk->getEnabled())
			{
				m_pAnimationState_Walk->setEnabled(true) ;
				m_pAnimationState_Stand->setEnabled(false) ;
			}
			m_pAnimationState_Walk->addTime(evt.timeSinceLastFrame) ;
		}
		else
		{
			if(!m_pAnimationState_Stand->getEnabled())
			{
				m_pAnimationState_Stand->setEnabled(true) ;
				m_pAnimationState_Walk->setEnabled(false) ;
			}
			m_pAnimationState_Stand->addTime(evt.timeSinceLastFrame) ;
		}
	}

	static float scale = 0.0f ;
	m_pLogoOverlay->setScale(abs(cos(scale)), abs(cos(scale))) ;
	scale += 0.01f ;

	static Ogre::DisplayString currFps = L"현재FPS" ;
	static Ogre::DisplayString avgFps = L"평균FPS" ;
	static Ogre::DisplayString bestFps = L"최고FPS" ;
	static Ogre::DisplayString worstFps = L"최저FPS" ;

	Ogre::OverlayElement *guiAvg = m_pOverlayMgr->getOverlayElement("AverageFps") ;
	Ogre::OverlayElement *guiCurr = m_pOverlayMgr->getOverlayElement("CurrFps") ;
	Ogre::OverlayElement *guiBest = m_pOverlayMgr->getOverlayElement("BestFps") ;
	Ogre::OverlayElement *guiWorst = m_pOverlayMgr->getOverlayElement("WorstFps") ;

	const RenderTarget::FrameStats& stats = m_pRoot->getAutoCreatedWindow()->getStatistics() ;

	guiAvg->setCaption(avgFps + StringConverter::toString(stats.avgFPS)) ;
	guiCurr->setCaption(currFps + StringConverter::toString(stats.lastFPS)) ;
	guiBest->setCaption(bestFps + StringConverter::toString(stats.bestFPS)) ;
	guiWorst->setCaption(worstFps + StringConverter::toString(stats.worstFPS)) ;

	return m_bContinue ;
}

bool PlayState::frameEnded(GameManager* game, const FrameEvent& evt)
{
	return true ;
}

bool PlayState::mouseMoved(GameManager* game, const OIS::MouseEvent &e)
{
	// 구면 카메라 설정
	m_pCameraYaw->yaw(Degree(-e.state.X.rel)) ;
	m_pCameraPitch->pitch(Degree(-e.state.Y.rel)) ;
	m_pCameraHolder->translate(Ogre::Vector3(0, 0, -e.state.Z.rel*0.1f)) ;

	return true ;
}

bool PlayState::mousePressed(GameManager* game, const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	return true ;
}

bool PlayState::mouseReleased(GameManager* game, const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	return true ;
}
	
bool PlayState::keyPressed(GameManager* game, const OIS::KeyEvent &e)
{
	switch(e.key)
	{
	case OIS::KC_W : case OIS::KC_UP : m_CharacterDirection.z += -1.0f ; break ;
	case OIS::KC_S : case OIS::KC_DOWN : m_CharacterDirection.z += 1.0f ; break ;
	case OIS::KC_A : case OIS::KC_LEFT : m_CharacterDirection.x += -1.0f ; break ;
	case OIS::KC_D : case OIS::KC_RIGHT : m_CharacterDirection.x += 1.0f ; break ;
	case OIS::KC_K :
		if(!m_bAttacking)
		{
			m_pAnimationState_Stand->setEnabled(false) ;
			m_pAnimationState_Walk->setEnabled(false) ;
			m_pAnimationState_Death->setEnabled(false) ;

			m_bAttacking = true ;
			m_pAnimationState_Attack->setLoop(false) ;
			m_pAnimationState_Attack->setEnabled(true) ;
		}
		break ;
	case OIS::KC_J :
		if(!m_bDeading)
		{
			m_pAnimationState_Stand->setEnabled(false) ;
			m_pAnimationState_Walk->setEnabled(false) ;
			m_pAnimationState_Attack->setEnabled(false) ;

			m_bDeading = true ;
			m_pAnimationState_Death->setLoop(false) ;
			m_pAnimationState_Death->setEnabled(true) ;
		}
		break ;
	case OIS::KC_ESCAPE : m_bContinue = false ; break ;
	}

	return true ;
}

bool PlayState::keyReleased(GameManager* game, const OIS::KeyEvent &e)
{
	switch(e.key)
	{
	case OIS::KC_W : case OIS::KC_UP : m_CharacterDirection.z -= -1.0f ; break ;
	case OIS::KC_S : case OIS::KC_DOWN : m_CharacterDirection.z -= 1.0f ; break ;
	case OIS::KC_A : case OIS::KC_LEFT : m_CharacterDirection.x -= -1.0f ; break ;
	case OIS::KC_D : case OIS::KC_RIGHT : m_CharacterDirection.x -= 1.0f ; break ;
	case OIS::KC_ESCAPE : m_bContinue = false ; break ;
	}

	return true ;
}