#ifndef PlayState_H
#define PlayState_H

#include <Ogre.h>
#include "GameState.h"

#include <OgreOverlay.h>
#include <OgreOverlayContainer.h>
#include <OgreOverlaySystem.h>
#include <OgreOverlayManager.h>

class PlayState : public GameState
{
public :
	void enter() ;
	void exit() ;

	void pause() ;
	void resume() ;

	bool frameStarted(GameManager* game, const Ogre::FrameEvent& evt) ;
	bool frameEnded(GameManager* game, const Ogre::FrameEvent& evt) ;
	
	bool mouseMoved(GameManager* game, const OIS::MouseEvent &e) ;
	bool mousePressed(GameManager* game, const OIS::MouseEvent &e, OIS::MouseButtonID id) ;
	bool mouseReleased(GameManager* game, const OIS::MouseEvent &e, OIS::MouseButtonID id) ;
	
	bool keyPressed(GameManager* game, const OIS::KeyEvent &e) ;
	bool keyReleased(GameManager* game, const OIS::KeyEvent &e) ;

	static PlayState* getInstance() { return &m_PlayState ; }

protected :
	PlayState() {}

	Ogre::Root*			m_pRoot ;
	Ogre::SceneManager*	m_pSceneMgr ;
	Ogre::Viewport		*m_pViewport ;
	Ogre::Camera		*m_pCamera ;

private :
	static PlayState m_PlayState ;

	Ogre::Entity			*m_pillidanEntity ;
	Ogre::SceneNode			*m_pillidanNode ;
	Ogre::SceneNode			*m_pillidanYaw ;

	Ogre::SceneNode			*m_pCameraHolder ;
	Ogre::SceneNode			*m_pCameraYaw ;
	Ogre::SceneNode			*m_pCameraPitch ;

	Ogre::Vector3			m_CameraMoveVector ;
	Ogre::Vector3			m_CharacterDirection ;

	Ogre::AnimationState	*m_pAnimationState_Walk ;
	Ogre::AnimationState	*m_pAnimationState_Stand ;
	Ogre::AnimationState	*m_pAnimationState_Attack ;
	Ogre::AnimationState	*m_pAnimationState_Death ;

	bool m_bAttacking, m_bDeading ;

	bool m_bContinue ;

	// GUI
	Ogre::OverlayManager		*m_pOverlayMgr ;

	Ogre::Overlay				*m_pTextOverlay ;
	Ogre::Overlay				*m_pLogoOverlay ;
	Ogre::Overlay				*m_pFpsOverlay ;

	Ogre::OverlayContainer		*m_pTextOverlayContainer ;
	Ogre::OverlayContainer		*m_pLogoOverlayContainer ;

	Ogre::OverlayElement		*m_pTextOverlayElement ;
} ;

#endif